#!/usr/bin/env python
# coding: utf-8

# In[1]:


import cv2
import numpy as np
from matplotlib import pyplot as plt


# In[2]:


img1 = cv2.imread('original image',cv2.IMREAD_COLOR)  #reading the images
img2 = cv2.imread('defected image',cv2.IMREAD_COLOR)
gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)        #converting to gray scale
gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
ret,thresh1 = cv2.threshold(gray1,200,255,cv2.THRESH_BINARY)  #applying threshold (binary) 
ret,thresh2 = cv2.threshold(gray2,200,255,cv2.THRESH_BINARY)
res =cv2.bitwise_xor(thresh1, thresh2, mask=None)      #comparing the images     
cv2.imshow('Bitwise XOR', res)
cv2.waitKey(0)
cv2.destroyAllWindows()
