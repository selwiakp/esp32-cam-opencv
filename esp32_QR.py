import cv2
import urllib.request
import numpy as np

url = #fill it
cv2.namedWindow("stream", cv2.WINDOW_AUTOSIZE)
detector = cv2.QRCodeDetector()

while True:
    img_resp = urllib.request.urlopen(url)
    imgnp = np.array(bytearray(img_resp.read()), dtype=np.uint8)
    frame = cv2.imdecode(imgnp, -1)

    data, bbox, _ = detector.detectAndDecode(frame)
    if data:
        print('Decoded data: ' + data)

    cv2.imshow("stream", frame)

    key = cv2.waitKey(5)
    if key == ord('q'):
        break
frame.release()
cv2.destroyAllWindows()