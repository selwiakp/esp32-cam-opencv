# ESP32-cam + OpenCV

Python 3.9, OpenCV 4, Arduino IDE

## Getting started

To run it, you have to add all necessary libraries to arduino IDE and fill WIFI_SSID and WIFI_PASS. Then you have to add to python files address of your local stream

## Author
Przemysław Selwiak, 2022
